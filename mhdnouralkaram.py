class DrUbaiBirthday:
    def create_wishes(self):
        return "Happy Birthday, Dr. Ubai! Wishing you a wonderful year!"
    
def main():
    dr_ubai_birthday = DrUbaiBirthday()
    wishes = dr_ubai_birthday.create_wishes()
    print(wishes)

if __name__ == "__main__":
    main()